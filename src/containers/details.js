/**
 * Created by bohdan on 20.08.18.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';

class Details extends Component {

    render() {
        if(!this.props.car) {
            return ( <p>Выберете автомобиль</p>);
        }
        return (
            <div>
                <h2>{this.props.car.name}</h2>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        car: state.active
    }
}

//используя connect мы указываем что иы так же выводим компонент с данными из хранилища
export default connect(
    mapStateToProps
)(Details);