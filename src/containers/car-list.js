/**
 * Created by bohdan on 20.08.18.
 */
// Отличие компонента от контейнера в том что контейнер берет информацию из хранилища, и помещает ее в контейнер

import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

//Импортируем Action - функцию для взаимодействия с хранилищем
import {selectCar} from '../actions/index.js';

class CarsList extends Component {
    showList () {
        return this.props.cars.map((car) => {
            return (
                <li onClick={ () => this.props.select (car) } key={car.id}>{car.name}</li>
            );
        });
    }

    render () {
        return (
            <ol>
                {this.showList()}
            </ol>
        )
    }
}

//Берет значение из массива cars и вставит их в компонет CarsList в качестве свойств
function mapStateToProps (state) {
    return {
        cars: state.cars  //Получаем полностью массив и можем его использовать
    }
}

//Зарезервированная функция matchDispatchToProps которая формирует Actions
//Единственный способ изменить состояние в store это функция dispach
//Здесь мы просто выстрелили событие
function matchDispatchToProps(dispatch) {
    //bindActionCreators функция которая связывает действие
    return bindActionCreators({ select: selectCar }, dispatch)
}

//используя connect мы указываем что иы так же выводим компонент с данными из хранилища
export default connect(
    mapStateToProps,
    matchDispatchToProps
)(CarsList);