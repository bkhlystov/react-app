/**
 * Created by bohdan on 20.08.18.
 */

//Функция описывающая действие для Actions
//Она просто говорит о том что мы используем функцию car_selectes
export const selectCar = (car) => {
    alert("Now car is" + car.name);
    return {
        type: "CAR_SELECTED",   //тригер события
        payload: car
    }
};