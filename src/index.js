import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


import { createStore } from 'redux'; //Импортируем Redux
import {Provider} from 'react-redux'; // Для связки React и Redux
import allReducers from './redusers/index.js';

const store = createStore(allReducers);


//Оборачивая центральный компонент в провайдер, мы дадим всем компонентам доступ к хранилищу
ReactDOM.render(
    <Provider store={store}>
        <App />
    </ Provider>,
    document.getElementById('root'));
registerServiceWorker();
