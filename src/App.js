import React, { Component } from 'react';
import Visual from './components/Visual.js';

class App extends Component {
  render() {
    return (
        // Когда React видит, что элемент представляет собой пользовательский компонент,
        // он передает все JSX-атрибуты в этот компонент единым объектом. Такой объект называется props.
        // то-есть будет доступно внутри компонета как this.props.name
      <Visual name="Sara"/>
    );
  }
}

export default App;
