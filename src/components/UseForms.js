/**
 * Created by bohdan on 17.08.18.
 */
import React, { Component } from 'react';

class UseForms extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            message: 'Текст сообщения'
        };

        this.onLoginChange = this.onLoginChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onMessageChange = this.onMessageChange.bind(this);
    }

    onSubmit(event){
        alert(`
            ${this.state.login}, добро пожаловать!
            текст ${this.state.message}
        `);
        event.preventDefault();
        this.setState({login: '', password: ''});
    }

    onPasswordChange(event){
        this.setState({password: event.target.value});
    }

    onLoginChange(event) {
        this.setState({login: event.target.value});
    }
    onMessageChange(event) {
        this.setState({message: event.target.value});
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <p>
                    <label> Логин:
                        <input type="text"
                               name="login"
                               value={this.state.login}
                               onChange={this.onLoginChange}
                        />
                    </label>
                </p>
                <p>
                    <label> Пароль:
                        <input type="password"
                               name="password"
                               value={this.state.password}
                               onChange={this.onPasswordChange}
                        />
                    </label>
                </p>
                <p>
                    <label>Текст сообщения:
                        <textarea type="text"
                                  name="message"
                                  value={this.state.message}
                                  onChange={this.onMessageChange}/>
                    </label>
                </p>
                <p>
                    <input type="submit" value="Submit" />
                </p>
            </form>
        );
    }
}

export default UseForms;