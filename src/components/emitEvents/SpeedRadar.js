/**
 * Created by bohdan on 17.08.18.
 */
import React, { Component } from 'react';
import SpeedSetter from './SpeedSetter.js';


class SpeedRadar extends Component {
    constructor(props){
        super(props);
        this.onSpeedInKphChange = this.onSpeedInKphChange.bind(this);
        this.onSpeedInMphChange = this.onSpeedInMphChange.bind(this);
        this.state = {speed: 0, unit: 'KPH'};
    }

    MAX_SPEED_IN_CITY_IN_KPH = 60;

    onSpeedInKphChange(speed) {
        this.setState({unit: 'KPH', speed});
    }

    onSpeedInMphChange(speed) {
        this.setState({unit: 'MPH', speed});
    }

    render() {
        const unit = this.state.unit;
        const speed = this.state.speed;
        const kph = unit === 'MPH' ? speed /*сonvertSpeed(speed, convertToKph)*/ : speed;
        const mph = unit === 'KPH' ? speed /*сonvertSpeed(speed, convertToMph)*/ : speed;

        return (
            <div>
                {/*Прописывая onSpeedChange мы подписались на изменения события в дочернем компоненте*/}
                <SpeedSetter unit="KPH" speed={kph} onSpeedChange={this.onSpeedInKphChange}/>
                <SpeedSetter unit="MPH" speed={mph} onSpeedChange={this.onSpeedInMphChange}/>
            </div>
        );
    }
}

export default SpeedRadar;