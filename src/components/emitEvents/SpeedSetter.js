/**
 * Created by bohdan on 17.08.18.
 */
import React, { Component } from 'react';

class SpeedSetter extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(e) {
        //пример emit события указываем название функции обработчика в родительском компоненте
        //и вызываем ее через props передавая туда даные
        this.props.onSpeedChange(e.target.value);
    }

    render() {
        const speed = this.props.speed;
        const unit = this.props.unit;
        return (
            <p>
                <span>Введите скорость: {unit}</span>
                <input value={speed} onChange={this.onChange} />
            </p>
        );
    }
}

export default SpeedSetter;