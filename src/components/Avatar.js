/**
 * Created by bohdan on 16.08.18.
 */
import React, { Component } from 'react';

class Avatar extends Component {
    render() {
        return (
            <img className="App-logo"
                 src={this.props.src}
                 alt="text"
            />
        );
    }
}

export default Avatar;