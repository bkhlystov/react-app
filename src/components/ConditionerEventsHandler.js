/**
 * Created by bohdan on 16.08.18.
 */
import React, { Component } from 'react';

//ПРИМЕР ИСПОЛТЬЗОВАНИЯ СОБЫТИЙ В JAVASCRIPT

class ConditionerEventsHandler extends Component {
    constructor(props) {
        super(props);
        this.state = {temperature: 0};

        // Привязка необходима, чтобы сделать this доступным в коллбэке
        this.onIncrease = this.onIncrease.bind(this);
        this.onDecrease = this.onDecrease.bind(this);
    }

    onIncrease(){
        this.setState(prevState => ({
            temperature: prevState.temperature + 1
        }))
    }

    onDecrease(){
        this.setState(prevState => ({
            temperature: prevState.temperature - 1
        }))
    }

    render() {
        return (<div>
            <h2>Текущая температура: {this.state.temperature}</h2>
            <button onClick={this.onDecrease}>-</button>
            <button onClick={this.onIncrease}>+</button>
        </div>);
    }
}

export default ConditionerEventsHandler;