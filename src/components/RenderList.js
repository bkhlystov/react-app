/**
 * Created by bohdan on 17.08.18.
 */
import React, { Component } from 'react';

class RenderList extends Component {

    render() {

        const users = [
            {id: 1, name: 'Вася'},
            {id: 2, name: 'Петя'},
            {id: 3, name: 'Ваня'}
        ];
        const messages = [
            {id: 1, message: 'Всем привет!', authorId: 1},
            {id: 2, message: 'И тебе привет!', authorId: 2},
            {id: 3, message: 'Привет, Вася :)', authorId: 3}
        ];

        const userList = (
            <p> Пользователи чата:
                {users.map((user) =>
                    <b key={user.id}> {user.name}; </b>
                )}
            </p>
        );
        const messageList = messages.map((message) => {
            let author = null;
            users.forEach((user) => {if(user.id === message.authorId) author = user});
            return (<p key={message.id}>
                <b>{author.name}: </b>
                <span>{message.message}</span>
            </p>)
        });

        return (
            <div>
                {userList}
                {messageList}
            </div>
        );


    }
}

export default RenderList;