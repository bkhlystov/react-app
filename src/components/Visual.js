/**
 * Created by bohdan on 16.08.18.
 */
import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';
import Avatar from "./Avatar";
import TimerWithMounted from './TimerWithMounted.js';
import ConditionerEventsHandler from './ConditionerEventsHandler.js';
import RenderList from './RenderList.js';
import UseForms from './UseForms.js';
import SpeedRadar from './emitEvents/SpeedRadar.js';

import CarsList from '../containers/car-list.js';
import Details from '../containers/details.js';

//В компонент свойства которые передают попадают в общий обьект this.props типа new Visual(props)
// props нельзя модифицировать как и в VueJS
class Visual extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    {/*ПЕРЕДАЧА props В КОМПОНЕНТ*/}
                    <Avatar src={logo}/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.js</code> and save to reload.
                </p>
                <h1>{this.props.name}</h1>

                {/*Работа с mounted*/}
                <div className="timer-app">
                    <TimerWithMounted />
                </div>

                {/*Обработчик событий*/}
                <div className="condicioner">
                    <ConditionerEventsHandler />
                </div>

                {/*Рендеринг списков*/}
                <div className="render-list">
                    <RenderList />
                </div>

                {/*Работа с формами*/}
                <div>
                    <UseForms />
                </div>

                {/*Пример как емитить собтия*/}
                <div className="emit-event">
                    <SpeedRadar />
                </div>

                {/*Пример с использованием Redux*/}
                <div>
                    <CarsList />
                    <Details />
                </div>
            </div>
        );
    }
}

export default Visual;