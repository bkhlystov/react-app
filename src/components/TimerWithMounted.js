/**
 * Created by bohdan on 16.08.18.
 */
import React, { Component } from 'react';

const INTERVAL = 100;

class TimerWithMounted extends Component {
    constructor(props) {
        super(props);

        //Состояние устанавливается локально для текущего компонента
        this.state = {value: 0};    //По аналогии как в VueJs метод data добавляем свои локальные свойства
    }

    //Добавление обычного метода
    // this.setState() СПЕЦИАЛЬЕЫЙ МЕТОД ДЛЯ ИЗМЕНЕНИЯ СОСТОЯНИЯ КОМПОНЕНТА, С ЕГО ПОСЛЕДУЮЩИМ ОБНОВЛЕНИЕМ
    // Благодаря вызову setState(), React знает, что состояние изменилось, и вызывает метод render() снова
    //  ПРИМЕР this.setState((prevState, props) => ({  // ДОБАВЛЯЕМ КОЛБЕК Ф-ЦИЮ ДЛЯ АСИНХРОННОГО ОБНОВЛЕНИЯ СОСТОЯНИЯ
    //                 temperature: prevState.temperature + props.delta
    //             }));
    increment(){
        this.setState({value: this.state.value + 1});
    }

    //СПЕЦИАЛЬНЫЙ МЕТОД ВЫЗОВЕТСЯ КОГДА МЕТОД МОНТИРУЕТСЯ
    componentDidMount() {
        this.timerID = setInterval(() => this.increment(), 1000/INTERVAL);
    }

    //СПЕЦИАЛЬНЫЙ МЕТОД ВЫЗОВЕТСЯ КОГДА КОМПОНЕНТ ДЕМОНТИРУЕТСЯ ИЗ DOM
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    render() {

        const value = this.state.value;

        return (
            <div>
                <p>Таймер:</p>
                <p>
                    <span>{Math.round(value/INTERVAL/60/60)} : </span>
                    <span>{Math.round(value/INTERVAL/60)} : </span>
                    <span>{Math.round(value/INTERVAL)} . </span>
                    <span>{value % INTERVAL}</span>
                </p>
            </div>
        );
    }
}

export default TimerWithMounted;

