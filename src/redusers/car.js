/**
 * Created by bohdan on 20.08.18.
 */
//Обьявленная функция которая должна работать с состоянием Redux
//называется функция reduser (Провайдер)
export default function () {
    return [
        {
            id: 1,
            name: 'Audi',
            speed: 234.45,
            weight: 1.4,
        },
        {
            id: 2,
            name: 'BMW',
            speed: 234.45,
            weight: 1.4,
        }
    ];
}