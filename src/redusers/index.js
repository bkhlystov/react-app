/**
 * Created by bohdan on 20.08.18.
 */
import {combineReducers} from 'redux';
import CarsReducers from './car';
import activeCare from './car-active.js';

//Обьеденяем редьюсеры в один обьект для передачи в центральное хранилище
const allReducers = combineReducers( {
    cars: CarsReducers,
    active: activeCare
});

export default allReducers;