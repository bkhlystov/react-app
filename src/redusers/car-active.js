/**
 * Created by bohdan on 20.08.18.
 */
//Приобразователь выполняет действие, в нем мы отследили action и выполняем действие
//При инициализации установит null, action действие которое мы принимаем
export default function(state=null, action) {
    switch (action.type) {
        case "CAR_SELECTED":
            return action.payload;
            break;
        default:
            return state;
    }
}